<?php

namespace AppBundle\Controller\Api;

use Symfony\Component\Form\FormTypeInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Request\ParamFetcherInterface;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use AppBundle\Entity\Customer;
use AppBundle\Form\CustomerType;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * REST controller for Customers
 * @package AppBundle\Controller\Api
 * @author Sergo Yakushenko <sergo.yakushenko@gmail.com>
 */

class CustomerController extends FOSRestController
{

    /**
     * @ApiDoc(
     *  resource = true,
     *  statusCodes={
     *      200 = "Successful"
     *  }
     * )
     * @Annotations\View()
     *
     * @Annotations\QueryParam(name="limit", requirements="\d+", default="5", description="How many Customers return.")
     */
    public function getCustomersAction(ParamFetcherInterface $paramFetcher)
    {
        $limit = $paramFetcher->get("limit");

        $customers = $this->getDoctrine()->getRepository('AppBundle:Customer')->findBy([],[],$limit);

        return ["customers" => $customers, "limit" => $limit];
    }

    /**
     * @Annotations\View()
     *
     * @return FormTypeInterface
     */
    public function newCustomerAction()
    {
        return $this->createForm(CustomerType::class, new Customer());
    }
    
    /**
     * Annotations\View(templateVar="customer")
     *
     * @param int $id
     *
     * @return array
     */
    public function getCustomerAction($id)
    {
        $customer = $this->getCustomerById($id);

        /* if use  Annotations\View(templateVar="customer") */
//        return $customer;

        $view = $this->view($customer, 200)
            ->setTemplate("AppBundle:Api/Customer:getCustomer.html.twig")
            ->setTemplateVar('customer');
        return $view;

        /* use FOS\RestBundle\View\View */
        /*
            $view = View::create();
            $view
                ->setData($customer)
                ->setTemplate("AppBundle:Api/Customer:getCustomer.html.twig")
                ->setTemplateVar('customer')
            ;
        return $view;
        or
        return $this->handleView($view);
        */
    }


    /**
     * Annotations\View(
     *   template = "AppBundle:Api/Customer:newCustomer.html.twig",
     *   statusCode = Response::HTTP_BAD_REQUEST
     * )
     *
     * @param Request $request the request object
     *
     * @return FormTypeInterface[]|View
     */
    public function postCustomerAction(Request $request)
    {
        $customer = new Customer();
        $form = $this->createForm(CustomerType::class, $customer);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $customer = $form->getData();

            $em = $this->getDoctrine()->getManager();
            $em->persist($customer);
            $em->flush();

            return $this->routeRedirectView('get_customer', ['id' => $customer->getId()]);
        }

        return ['form' => $form ];
    }

    /**
     * @Annotations\View()
     *
     * @param int $id
     *
     * @return FormTypeInterface
     */
    public function editCustomerAction($id)
    {
        $customer = $this->getCustomerById($id);

        return $this->createForm(CustomerType::class, $customer);
    }

    /**
     * @Annotations\View(
     *      template = "AppBundle:Api/Customer:getCustomer.html.twig",
     *      templateVar = "customer"
     * )
     * @param Request $request
     * @param int $id
     * @return View|FormTypeInterface
     */
    public function putCustomerAction(Request $request, $id)
    {
        $customer = $this->getCustomerById($id);

        if( $customer === false) {
            $customer = new Customer();
            $statusCode = Response::HTTP_CREATED;
        } else {
            $statusCode = Response::HTTP_NO_CONTENT;
        }

        $form = $this->createForm( CustomerType::class, $customer);

        $form->submit($request->request->get($form->getName())); // $form->handleRequest($request); - Doesn't work with PUT

        if( $form->isValid()) {
            $customer = $form->getData();

            $em = $this->getDoctrine()->getManager();
            $em->persist($customer);
            $em->flush();

            return $this->routeRedirectView('get_customer', ['id' => $customer->getId()], $statusCode);
        }

        return $form;
    }

    /**
     * @param int $id
     * @return View
     */
    public function removeCustomerAction($id)
    {
        return $this->deleteCustomerAction($id);
    }

    /**
     * @param int $id
     * @return View
     */
    public function deleteCustomerAction($id)
    {
        $customer = $this->getCustomerById($id);

        $em = $this->getDoctrine()->getManager();
        $em->remove($customer);
        $em->flush();

        return $this->routeRedirectView('get_customers');
    }

    /**
     * @param $id
     * @throws NotFoundHttpException
     * @return object Customer
     */
    public function getCustomerById($id){
        $customer = $this->getDoctrine()->getRepository('AppBundle:Customer')->find($id);

        if($customer === false) {
            throw $this->createNotFoundException("Cant't find Customer with id = {id}");
        }
        return $customer;
    }

}
